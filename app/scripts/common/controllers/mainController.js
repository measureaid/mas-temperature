(function(){
    'use strict';

    angular
        .module('masApp')
        .controller('MainController', ControllerCtrl)

    function ControllerCtrl($rootScope, $scope){
        var vm = this;

        $rootScope.$on('inputTemp', function(err, data){
          $scope.inputTemp = data;
          $scope.$apply();
        });
        $rootScope.$on('outputTemp', function(err, data){
          $scope.outputTemp = data;
          $scope.$apply();
        });        

    }

}());