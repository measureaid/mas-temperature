/* globals FileAPI, io, $ */
(function () {
	'use strict';

	angular
		.module('masApp', [
			'mas.config',
			'ui.router',
			// 'dependencies'
		])



	.run(function ($rootScope) {
		const ipc = require('electron').ipcRenderer;
		ipc.on('barcode', function (event, data) {
			// console.log(data);
			$rootScope.$broadcast('barcode', data);
			//  document.getElementById('barcode-result').innerHTML = data;
		});
		ipc.on('inputTemp', function (event, data) {
			// console.log(data);
			$rootScope.$broadcast('inputTemp', data);
			//  document.getElementById('barcode-result').innerHTML = data;
		});
		ipc.on('outputTemp', function (event, data) {
			// console.log(data);
			$rootScope.$broadcast('outputTemp', data);
			//  document.getElementById('barcode-result').innerHTML = data;
		});
	});

	// .run(function ($rootScope, $state, Auth, ACCESS_LEVELS, AUTH_STATE_HOME, AUTH_STATE_LOGIN) {
	// 	$rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState) {
	// 		var targetAuth = (toState.hasOwnProperty('data')) ? toState.data.access : ACCESS_LEVELS.public;
	// 		if (!Auth.authorize(targetAuth)) {
	// 			$rootScope.error = 'Seems like you tried accessing a route you don\'t have access to...';
	// 			event.preventDefault();
	//
	// 			if (fromState.url === '^') {
	// 				if (Auth.isLoggedIn()) {
	// 					$state.go(AUTH_STATE_HOME);
	// 				} else {
	// 					$rootScope.error = null;
	// 					$state.go(AUTH_STATE_LOGIN);
	// 				}
	// 			}
	// 		}
	// 	});
	// })

	// .run(function (BASE_URL, $rootScope, masService, hubService, Auth, $state, toastr, Production) {
	// 	$rootScope.connection = 0;
	// 	io.sails.url = global.BASE_URL;
	// 	io.sails.transports = ['websocket'];
	// 	var socket = io.sails.connect();

	// 	io.socket.on('connect', function () {
	// 		console.log('접속됨, 내가 누구인지 알아보자');
	// 		toastr.info('서버에 접속되었습니다.');
	// 		require('getmac').getMac(function (err, macAddress) {
	// 			hubService.whoAmI(macAddress);
	// 		});
	// 		$rootScope.connection = 1;
	// 	});

	// 	io.socket.on('disconnect', function () {
	// 		console.log('접속끊김');
	// 		toastr.info('서버 연결이 끊겼습니다');
	// 		$rootScope.connection = 0;
	// 		//로그아웃하고
	// 		Auth.logout(function () {
	// 			$state.go('public.signin');
	// 			//허브 정보 없에고
	// 			$rootScope.hub = {};
	// 			$rootScope.$apply();
	// 		});
	// 	});

	// 	masService.socket(socket);
	// })

	// .config(function ($httpProvider) {
	// 	$httpProvider.defaults.withCredentials = true;
	// 	//rest of route code
	// })

	// .run(function ($injector, $rootScope, BASE_URL, MAS_C, MAS_H) {
	// 	$rootScope.BASE_URL = BASE_URL;
	// 	$rootScope.IMAGE_BASE_URL = BASE_URL;
	// 	$rootScope.MAS_C = MAS_C;
	// 	$rootScope.MAS_H = MAS_H;
	// });
})();
