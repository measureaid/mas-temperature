const path = require('path');
// const glob = require('glob');
const electron = require('electron');
const autoUpdater = require('./auto-updater');

const BrowserWindow = electron.BrowserWindow;
const app = electron.app;

const debug = /--debug/.test(process.argv[2]);
// var debug = true;
var mainWindow = null;


var socketIOClient = require('socket.io-client');
var sailsIOClient = require('sails.io.js');
var io = sailsIOClient(socketIOClient);
io.sails.url = 'http://demo-backend.measureaid.com';
io.sails.transports = ['websocket'];
//
// io.socket.on('connected', function (timeDisconnected, reconnectCount) {
// 	console.log('접속됨');
//
// });

function sendServer(type, value) {
	io.socket.post('/sensor', {
		type: type,
		value: value
	}, function () {
		console.log(arguments);
	});
}

if(process.platform === 'linux') {
	var wpi = require('wiring-pi');

	wpi.setup('wpi');

	wpi.pinMode(3, wpi.OUTPUT);
	wpi.digitalWrite(3, wpi.LOW);
	setTimeout(function () {
		console.log('turn on');
		wpi.digitalWrite(3, wpi.HIGH);
	}, 1000);
	setTimeout(function () {
		console.log('turn off');
		wpi.digitalWrite(3, wpi.LOW);
		//	wpi.pinMode(3, wpi.INPUT);
	}, 2000);
	
} else if(process.platform === 'darwin') {
	setInterval(function () {
		var inputTemp = 20 + (Math.random() * 10);
		console.log('온도계(입수)', inputTemp);
		mainWindow.webContents.send('inputTemp', inputTemp);
		sendServer('inputTemp', inputTemp);
	}, 600);
	setInterval(function () {
		var outputTemp = 20 + (Math.random() * 10);
		console.log('온도계(출수)', outputTemp);

		mainWindow.webContents.send('outputTemp', outputTemp);
		sendServer('outputTemp', outputTemp);
	}, 600);

}

function initialize() {
	var shouldQuit = makeSingleInstance();
	if(shouldQuit) return app.quit();

	// loadDemos();
	function createWindow() {
		var windowOptions = {
			width: 800,
			// minWidth: 680,
			height: 600,
			show: true,
			// debug:true,
			'fullscreen': true,
			'frame': false,
			'kiosk': true,
		};
		if(process.platform === 'linux') {
			windowOptions.icon = path.join(__dirname, '/assets/app-icon/png/512.png');
		}

		mainWindow = new BrowserWindow(windowOptions);
		mainWindow.loadURL(path.join('file://', __dirname, 'index.html'));
		// Launch fullscreen with DevTools open, usage: npm run debug
		if(debug) {
			mainWindow.webContents.openDevTools();
			mainWindow.maximize();
		}

		if(process.platform === 'linux') {

			var ds18b20 = require('ds18b20');


			setInterval(function () {
				// var input_temp = Math.random() * 100;


				var input_temp = ds18b20.temperatureSync('28-0315746f08ff');
				mainWindow.webContents.send('inputTemp', input_temp);
				sendServer('inputTemp', input_temp);

				if(input_temp > 40) {
					console.log('Alarm');
					wpi.pinMode(3, wpi.OUTPUT);
				} else {
					console.log('Dealarm');
					wpi.pinMode(3, wpi.INPUT);
				}
				var output_temp = ds18b20.temperatureSync('28-01157336a9ff');
				mainWindow.webContents.send('outputTemp', output_temp);
				sendServer('outputTemp', output_temp);
			}, 250);
		}



		mainWindow.on('closed', function () {
			mainWindow = null;
		});
	}

	app.on('ready', function () {
		createWindow();
		autoUpdater.initialize();
	});

	app.on('window-all-closed', function () {
		if(process.platform !== 'darwin') {
			app.quit();
		}
	});

	app.on('activate', function () {
		if(mainWindow === null) {
			createWindow();
		}
	});
}

// Make this app a single instance app.
//
// The main window will be restored and focused instead of a second window
// opened when a person attempts to launch a second instance.
//
// Returns true if the current version of the app should quit instead of
// launching.
function makeSingleInstance() {
	return app.makeSingleInstance(function () {
		if(mainWindow) {
			if(mainWindow.isMinimized()) mainWindow.restore();
			mainWindow.focus();
		}
	});
}

// Require each JS file in the main-process dir
// function loadDemos() {
// 	var files = glob.sync(path.join(__dirname, 'main-process/**/*.js'));
// 	files.forEach(function (file) {
// 		require(file);
// 	});
// 	autoUpdater.updateMenu();
// }

// Handle Squirrel on Windows startup events
switch(process.argv[1]) {
case '--squirrel-install':
	autoUpdater.createShortcut(function () {
		app.quit();
	});
	break;
case '--squirrel-uninstall':
	autoUpdater.removeShortcut(function () {
		app.quit();
	});
	break;
case '--squirrel-obsolete':
case '--squirrel-updated':
	app.quit();
	break;
default:
	initialize();
}
