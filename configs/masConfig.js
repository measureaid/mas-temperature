(function () {
	'use strict';

	angular
		.module('mas.config', [
			'ui.router',
			'toastr',

		])

	/******************* local storage prefix *************/
	.constant('PREFIX', 'mas')
		/********************* backend url ********************/
		.constant('BASE_URL', global.BASE_URL)
		.constant('IMG_BASE_URL', '/api/')
		.constant('MAS_CONFIG', {
			title: 'MeasureAid'
		})
		.config(
			function ($stateProvider) {

				$stateProvider
					.state('public', {
						abstract: true,
						url: '',
						template: '<ui-view/>',
						// controller: 'mainController',
					})
					.state('public.main', {
						url: '/',
						templateUrl: 'app/views/main.html',
						controller: 'MainController',
						controllerAs: 'vm',
					})

				;

			}
		);


})();
