angular
  .module('richu.auth.config', [])
  .constant('AUTH_BASE_URL', global.BASE_URL)

.constant('AUTH_PREFIX', 'ma')
  .constant('AUTH_STATE_HOME', 'user.home')
  .constant('AUTH_STATE_LOGIN', 'anon.login')
  .constant('AUTH_SERVER_CONFIG', {
    'register': {
      method: 'POST',
      params: {
        auth: 'auth',
        provider: 'local',
        action: 'register'
      }
    },
    'login': {
      method: 'POST',
      params: {
        auth: 'auth',
        provider: 'local'
      }
    },
    'loginByBarcode': {
      method: 'POST',
      params: {
        auth: 'auth',
        provider: 'barcode'
      }
    },
    'findpassword': {
      method: 'POST',
      params: {
        action: 'user',
        actionId: 'findpassword',
      }
    },
    'resetpassword': {
      method: 'POST',
      params: {
        auth: 'auth',
        provider: 'local',
        action: 'reset'
      }
    },
    'checkUser': {
      method: 'GET',
      params: {
        action: 'user',
        actionId: 'check'
      }
    },
    'viewUser': {
      method: 'GET',
      params: {
        action: 'user'
      }
    },
    'updateUser': {
      method: 'PUT',
      params: {
        action: 'user'
      }
    },
  })


;
