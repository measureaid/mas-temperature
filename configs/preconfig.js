/****** GLOBAL AREA *********/
var _DEBUG = true;
if (_DEBUG === false) {
  if (typeof console !== 'undefined') {
    console.log = function() {};
    console.debug = function() {};
    console.info = function() {};
  }
} else {
  if (typeof console === 'undefined') {
    var f = function() {};
    window.console = {
      log: f,
      info: f,
      warn: f,
      debug: f,
      error: f
    };
  }
}

var authConfig = {
  roles: [
    'public',
    'user',
    'lecturer',
    'orgadmin',
    'admin'
  ],
  accessLevels: {
    'public': '*',
    'anon': ['public'],
    'user': ['user', 'lecturer', 'orgadmin', 'admin'],
    'lecturer': ['lecturer', 'orgadmin', 'admin'],
    'manager': ['orgadmin', 'admin'],
    'admin': ['admin']
  }
};
var global = {};

// global.BASE_URL = '/api/';
global.BASE_URL = 'http://dev.shcm.co.kr/';


/****** END OF GLOBAL AREA *********/
